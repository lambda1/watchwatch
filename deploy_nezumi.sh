#!/bin/bash

rm function.zip
cd package
zip -r9 ${OLDPWD}/function.zip .

cd $OLDPWD
zip -g function.zip nezumi.py

aws lambda update-function-code --function-name watchwatch_nezumi --zip-file fileb://function.zip