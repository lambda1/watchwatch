#!/bin/bash
source app-env.sh

docker run -it --rm -e BOT_TOKEN="$BOT_TOKEN" -e BOT_CHATID="$BOT_CHATID" -e PYTHONPATH="package" -v  "$PWD":/var/task lambci/lambda:python3.7 nezumi.handler
