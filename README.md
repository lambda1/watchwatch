# Watch Watch
## Keeping an eye on watches with AWS Lambda

This AWS Lambda function triggers every day to check the price of a few watches I'm interested in, it's easily configurable/extensible as it sources an Airtable Base as a config . But it can be used for anything, all it requires is an URL, and Xpath to isolate the element you want to compare and an expected value, if the value is different than what it's expecting it will send you a telegram message .

---


### Configuration

#### Airtable

You will need an [Airtable](https://www.airtable.com) base with these columns :

- Name
- Url
- Xpath
- Expected Return Value

#### ENV variables

For testing *run.sh* is using a Docker image to mimic the Lambda environment, we need these ENV variables :

- **BASEID** , your airtable base id
- **AIRTABLE_API_KEY**, your airtable api key
- **BOT_TOKEN**, telegram bot token
- **BOT_CHATID**, telegram chat id

### Packaging and Deployment

*deploy.sh* packages and deploys to Lambda, you'll want to tweak the shell command to match your function name .
