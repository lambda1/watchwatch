import requests
import airtable
import os
import logging
from lxml import html

logging.basicConfig(level="INFO")

tableid = os.environ['BASEID']
user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
headers = {'User-Agent': user_agent}


def get_airtable_data(tableid):
    data = airtable.Airtable(tableid, "Table 1")
    results = []
    for i in data.get_all():
        results.append(i['fields'])

    return results


def check_price(watch):
    url = watch['Url']
    page = requests.get(url, headers=headers)
    tree = html.fromstring(page.content)

    price = tree.xpath(watch['Xpath'])
    print(price)
    if price[0].strip() == watch['Expected Return Value']:
        logging.info(f"{watch['Name']} is the same price")
        return True

    else:
        return False


def telegram_bot_send_message(watch):
    """
    Publishes new record to Telegram
    Returns : Telegram's response
    """
    bot_token = os.environ['BOT_TOKEN']
    bot_chatID = os.environ['BOT_CHATID']
    bot_message = f"[{watch['Name']}]({watch['Url']}) seems to have a different price. "
    send_text = f'https://api.telegram.org/bot{bot_token}/sendMessage?chat_id={bot_chatID}&parse_mode=Markdown&text={bot_message}'
    response = requests.get(send_text)

    return response.json()


def handler(context, event):
    for i in get_airtable_data(tableid):
        logging.info(i)
        print(i)
        if check_price(i) is False:
            telegram_bot_send_message(i)
