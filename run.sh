#!/bin/bash
source app-env.sh

docker run -it --rm -e BASEID="$BASEID" -e AIRTABLE_API_KEY="$AIRTABLE_API_KEY" -e BOT_TOKEN="$BOT_TOKEN" -e BOT_CHATID="$BOT_CHATID" -e PYTHONPATH="package" -v  "$PWD":/var/task lambci/lambda:python3.7 function.handler
