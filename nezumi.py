import requests
import os
from lxml import html


def check_nezumi():

    url = "https://www.nezumistudios.com/product/loews-ref-lq1-201/"
    xpath = '/html/body/section/div/div[1]/div[2]/div[2]/div/form/div/div[1]/div[2]/span/span/text()'
    expected_price = "3300"
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    headers = {'User-Agent': user_agent}

    page = requests.get(url, headers=headers)
    tree = html.fromstring(page.content)
    values = tree.forms[1].values()
    content = tree.forms[1].text_content().strip()
    price = content.split("NON-EU customer ( ")[1].split(" SEK")[0]
    
    if price == expected_price:
        return True
    else:
        return False


def telegram_bot_send_message(watch):
    """
    Publishes new record to Telegram
    Returns : Telegram's response
    """
    bot_token = os.environ['BOT_TOKEN']
    bot_chatID = os.environ['BOT_CHATID']
    bot_message = f"[{watch['Name']}]({watch['Url']}) seems to have a different price. "
    send_text = f'https://api.telegram.org/bot{bot_token}/sendMessage?chat_id={bot_chatID}&parse_mode=Markdown&text={bot_message}'
    response = requests.get(send_text)

    return response.json()


def handler(context, event):
    if check_nezumi() is False:
        telegram_bot_send_message(i)





